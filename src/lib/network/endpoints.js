const api = '/api'

export const SIGN_IN = `${api}/auth/signin/password`
export const SIGN_UP = `${api}/auth/signup/password`

// Organisations
export const FETCH_ORGANISATIONS = `${api}/organisations`
export const FETCH_ORGANISATION = `${api}/organisations/single`
export const FETCH_USER_ORGANISATIONS = `${api}/organisations/me`
export const CREATE_ORGANISATION = `${api}/organisations/create`
export const DELETE_ORGANISATION = `${api}/organisations/single`

// Categories
export const FETCH_CATEGORY_OPTS = `${api}/categories/options`
export const FETCH_CATEGORIES_GRAPH = `${api}/categories/graph`

// Subcategories
export const FETCH_SUBCATEGORIES_BY_CATEGORY_OPTS = `${api}/categories/subcategories/by-category/options`

// Products
export const FETCH_PRODUCTS = `${api}/products/`
export const FETCH_PRODUCT = `${api}/products/single`
export const CREATE_PRODUCT = `${api}/products/`

// templates
export const FETCH_TEMPLATE_BY_SUBCATEGORY=`${api}/templates/single/by-subcategory`

// cart items
export const CREATE_CART_ITEM = `${api}/cart-items/`
export const DELETE_CART_ITEM = `${api}/cart-items/`
export const FETCH_USER_CART_ITEMS = `${api}/cart-items/me`