import { notification } from 'antd'

const error = ( data )=>{
    if(typeof data ==="string"){
        data = { message: data }
    }

    const args = {
        message: 'Eroare',
        description: data.message
    };
    return notification.error(args);
}

const success = ( data )=>{
    if(typeof data ==="string"){
        data = { message: data }
    }

    const args = {
        message: 'Success',
        description: data?.message
    }
      
    return notification.success(args);
}

export default {
    success,
    error,
}