export default {
  // Products
  FETCH_PRODUCTS_BEGIN: "Fetch Products Begin",
  FETCH_PRODUCTS_FAILURE: "Fetch Products Failure",
  FETCH_PRODUCTS_SUCCESS: "Fetch Products Success",

  FETCH_TOP_PRODUCTS_BEGIN: "Fetch Top Products Begin",
  FETCH_TOP_PRODUCTS_FAILURE: "Fetch Top Products Failure",
  FETCH_TOP_PRODUCTS_SUCCESS: "Fetch Top Products Success",

  FETCH_PRODUCT_BEGIN: "Fetch Product Begin",
  FETCH_PRODUCT_FAILURE: "Fetch Product Failure",
  FETCH_PRODUCT_SUCCESS: "Fetch Product Success",

  UPDATE_PRODUCTS_OPTIONS: "Update Products Options",
  UPDATE_PRODUCTS_FILTERS: "Update Products Filters",
}
