
import {
  GET,
} from "lib/network/requests"
import {
  FETCH_PRODUCTS as getProductsUrl,
  FETCH_PRODUCT as getProductUrl,
} from "lib/network/endpoints"
import _ from "underscore"
import notification from "lib/helpers/notification"
import actions from "./names"

// Products
export const getProducts = () => (dispatch, getState) => {
  dispatch({ type: actions.FETCH_PRODUCTS_BEGIN })

  const { productsOptions, productsFilters } = getState()
  const data = { ...productsOptions, ...productsFilters }

  GET({ url: getProductsUrl, data }, (err, res) => {
    if (err) {
      return notification.error(err.message)
    }

    dispatch({
      type: actions.FETCH_PRODUCTS_SUCCESS,
      products: res.products,
      total: res.total,
    })
  })
}

export const getProduct = (_id) => (dispatch) => {
  dispatch({ type: actions.FETCH_PRODUCT_BEGIN })

  GET({ url: getProductUrl, data: { _id } }, (err, product) => {
    if (err) {
      return notification.error(err.message)
    }

    dispatch({
      type: actions.FETCH_PRODUCT_SUCCESS,
      product,
    })
  })
}

export const updateProductsOptions = (options) => (dispatch) => {
  dispatch({ type: actions.UPDATE_PRODUCTS_OPTIONS, options })
  dispatch(getProducts())
}

export const updateProductsFilters = (filters) => (dispatch) => {
  dispatch({ type: actions.UPDATE_PRODUCTS_OPTIONS, options: { page: 1 } })
  dispatch({ type: actions.UPDATE_PRODUCTS_FILTERS, filters })
  dispatch(getProducts())
}
