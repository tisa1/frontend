import actions from "./names"

export const setUserToken = (userToken) => ({
  type: actions.SET_USER_TOKEN,
  userToken,
});