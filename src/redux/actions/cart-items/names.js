export default {
  // Products
  FETCH_USER_CART_ITEMS_BEGIN: "Fetch User Cart Items Begin",
  FETCH_USER_CART_ITEMS_FAILURE: "Fetch User Cart Items Failure",
  FETCH_USER_CART_ITEMS_SUCCESS: "Fetch User Cart Items Success",

}
