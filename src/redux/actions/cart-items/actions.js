
import {
  GET,
} from "lib/network/requests"
import {
  FETCH_USER_CART_ITEMS as getUserCartItemsUrl,
} from "lib/network/endpoints"
import _ from "underscore"
import notification from "lib/helpers/notification"
import actions from "./names"

// CartItems
export const getUserCartItems = () => (dispatch, getState) => {
  dispatch({ type: actions.FETCH_USER_CART_ITEMS_BEGIN })

  const { cartItemsOptions, cartItemsFilters } = getState()
  const data = { ...cartItemsOptions, ...cartItemsFilters }

  GET({ url: getUserCartItemsUrl, data }, (err, res) => {
    console.log({err,res})
    if (err) {
      return notification.error(err.message)
    }

    dispatch({
      type: actions.FETCH_USER_CART_ITEMS_SUCCESS,
      cartItems: res.cartItems,
      total: res.total,
    })
  })
}

// export const getCartItem = (_id) => (dispatch) => {
//   dispatch({ type: actions.FETCH_USER_CART_ITEM_BEGIN })

//   GET({ url: getCartItemUrl, data: { _id } }, (err, cartItem) => {
//     if (err) {
//       return notification.error(err.message)
//     }

//     dispatch({
//       type: actions.FETCH_USER_CART_ITEM_SUCCESS,
//       cartItem,
//     })
//   })
// }

export const updateCartItemsOptions = (options) => (dispatch) => {
  dispatch({ type: actions.UPDATE_USER_CART_ITEMS_OPTIONS, options })
  dispatch(getUserCartItems())
}

export const updateCartItemsFilters = (filters) => (dispatch) => {
  dispatch({ type: actions.UPDATE_USER_CART_ITEMS_OPTIONS, options: { page: 1 } })
  dispatch({ type: actions.UPDATE_USER_CART_ITEMS_FILTERS, filters })
  dispatch(getUserCartItems())
}
