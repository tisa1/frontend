export default {
  // Categories
  FETCH_CATEGORIES_BEGIN: "Fetch Categories Begin",
  FETCH_CATEGORIES_FAILURE: "Fetch Categories Failure",
  FETCH_CATEGORIES_SUCCESS: "Fetch Categories Success",

  FETCH_CATEGORIES_GRAPH_BEGIN: "Fetch Categories Graph Begin",
  FETCH_CATEGORIES_GRAPH_FAILURE: "Fetch Categories Graph Failure",
  FETCH_CATEGORIES_GRAPH_SUCCESS: "Fetch Categories Graph Success",

  FETCH_TOP_CATEGORIES_BEGIN: "Fetch Top Categories Begin",
  FETCH_TOP_CATEGORIES_FAILURE: "Fetch Top Categories Failure",
  FETCH_TOP_CATEGORIES_SUCCESS: "Fetch Top Categories Success",

  FETCH_CATEGORY_BEGIN: "Fetch Category Begin",
  FETCH_CATEGORY_FAILURE: "Fetch Category Failure",
  FETCH_CATEGORY_SUCCESS: "Fetch Category Success",

  UPDATE_CATEGORIES_OPTIONS: "Update Categories Options",
  UPDATE_CATEGORIES_FILTERS: "Update Categories Filters",
}
