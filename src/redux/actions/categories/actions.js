
import {
  POST, GET, PUT, DELETE,
} from "lib/network/requests"
import {
  FETCH_CATEGORIES_GRAPH as getCategoriesGraphUrl,
  // FETCH_CATEGORY as getCategoryUrl,
} from "lib/network/endpoints"

import _ from "underscore"
import notification from "lib/helpers/notification"
import actions from "./names"

// Categories
export const getCategoriesGraph = () => (dispatch, getState) => {
  dispatch({ type: actions.FETCH_CATEGORIES_GRAPH_BEGIN })

  const { categoriesGraphOptions, categoriesGraphFilters } = getState()
  const data = { ...categoriesGraphOptions, ...categoriesGraphFilters }

  GET({ url: getCategoriesGraphUrl, data }, (err, res) => {
    if (err) {
      return notification.error(err.message)
    }

    dispatch({
      type: actions.FETCH_CATEGORIES_GRAPH_SUCCESS,
      categories: res.categories,
      total: res.total,
    })
  })
}

export const getCategories = () => (dispatch, getState) => {
  dispatch({ type: actions.FETCH_CATEGORIES_BEGIN })

  const { categoriesOptions, categoriesFilters } = getState()
  const data = { ...categoriesOptions, ...categoriesFilters }

  GET({ url: getCategoriesUrl, data }, (err, res) => {
    if (err) {
      return notification.error(err.message)
    }

    dispatch({
      type: actions.FETCH_CATEGORIES_SUCCESS,
      categories: res.categories,
      total: res.total,
    })
  })
}

// export const getCategory = (_id) => (dispatch) => {
//   dispatch({ type: actions.FETCH_CATEGORY_BEGIN })

//   GET({ url: getCategoryUrl, data: { _id } }, (err, organisation) => {
//     if (err) {
//       return notification.error(err.message)
//     }

//     dispatch({
//       type: actions.FETCH_CATEGORY_SUCCESS,
//       organisation,
//     })
//   })
// }

export const updateCategoriesOptions = (options) => (dispatch) => {
  dispatch({ type: actions.UPDATE_CATEGORIES_OPTIONS, options })
  dispatch(getCategories())
}

export const updateCategoriesFilters = (filters) => (dispatch) => {
  dispatch({ type: actions.UPDATE_CATEGORIES_OPTIONS, options: { page: 1 } })
  dispatch({ type: actions.UPDATE_CATEGORIES_FILTERS, filters })
  dispatch(getCategories())
}
