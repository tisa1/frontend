export default {
  // Organisations
  FETCH_ORGANISATIONS_BEGIN: "Fetch Organisations Begin",
  FETCH_ORGANISATIONS_FAILURE: "Fetch Organisations Failure",
  FETCH_ORGANISATIONS_SUCCESS: "Fetch Organisations Success",

  FETCH_TOP_ORGANISATIONS_BEGIN: "Fetch Top Organisations Begin",
  FETCH_TOP_ORGANISATIONS_FAILURE: "Fetch Top Organisations Failure",
  FETCH_TOP_ORGANISATIONS_SUCCESS: "Fetch Top Organisations Success",

  FETCH_ORGANISATION_BEGIN: "Fetch Organisation Begin",
  FETCH_ORGANISATION_FAILURE: "Fetch Organisation Failure",
  FETCH_ORGANISATION_SUCCESS: "Fetch Organisation Success",

  UPDATE_ORGANISATIONS_OPTIONS: "Update Organisations Options",
  UPDATE_ORGANISATIONS_FILTERS: "Update Organisations Filters",
}
