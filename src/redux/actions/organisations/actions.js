
import {
  POST, GET, PUT, DELETE,
} from "lib/network/requests"
import {
  FETCH_ORGANISATIONS as getOrganisationsUrl,
  FETCH_ORGANISATION as getOrganisationUrl,
} from "lib/network/endpoints"

import _ from "underscore"
import { NotificationManager } from "react-notifications"
import actions from "./names"

// Organisations
export const getOrganisations = () => (dispatch, getState) => {
  dispatch({ type: actions.FETCH_ORGANISATIONS_BEGIN })

  const { organisationsOptions, organisationsFilters } = getState()
  const data = { ...organisationsOptions, ...organisationsFilters }

  GET({ url: getOrganisationsUrl, data }, (err, res) => {
    if (err) {
      return NotificationManager.error(err.message)
    }

    dispatch({
      type: actions.FETCH_ORGANISATIONS_SUCCESS,
      organisations: res.organisations,
      total: res.total,
    })
  })
}

export const getOrganisation = (_id) => (dispatch) => {
  dispatch({ type: actions.FETCH_ORGANISATION_BEGIN })

  GET({ url: getOrganisationUrl, data: { _id } }, (err, organisation) => {
    if (err) {
      return NotificationManager.error(err.message)
    }

    dispatch({
      type: actions.FETCH_ORGANISATION_SUCCESS,
      organisation,
    })
  })
}

export const updateOrganisationsOptions = (options) => (dispatch) => {
  dispatch({ type: actions.UPDATE_ORGANISATIONS_OPTIONS, options })
  dispatch(getOrganisations())
}

export const updateOrganisationsFilters = (filters) => (dispatch) => {
  dispatch({ type: actions.UPDATE_ORGANISATIONS_OPTIONS, options: { page: 1 } })
  dispatch({ type: actions.UPDATE_ORGANISATIONS_FILTERS, filters })
  dispatch(getOrganisations())
}
