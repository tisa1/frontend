import initialState from "./initialState"
import actions from "../../actions/organisations/names"
import _ from "underscore"

// ORGANISATIONS
export const organisations = (state = initialState.organisations, action) => {
  switch (action.type) {
  case actions.FETCH_ORGANISATIONS_SUCCESS:
    return action.organisations
  default:
    return state
  }
}

export const organisationsTotal = (state = initialState.organisationsTotal, action) => {
  switch (action.type) {
  case actions.FETCH_ORGANISATIONS_SUCCESS:
    return action.total
  default:
    return state
  }
}

export const organisationsLoading = (state = initialState.organisationsLoading, action) => {
  switch (action.type) {
  case actions.FETCH_ORGANISATIONS_BEGIN:
    return true
  case actions.FETCH_ORGANISATIONS_SUCCESS:
  case actions.FETCH_ORGANISATIONS_FAILURE:
    return false
  default:
    return state
  }
}

export const organisationsOptions = (state = initialState.organisationsOptions, action) => {
  switch (action.type) {
  case actions.UPDATE_ORGANISATIONS_OPTIONS:
    return _.extend(state, action.options)
  default:
    return state
  }
}

export const organisationsFilters = (state = initialState.organisationsFilters, action) => {
  switch (action.type) {
  case actions.UPDATE_ORGANISATIONS_FILTERS:
    return _.extend(state, action.filters)
  default:
    return state
  }
}

// ORGANISATION SINGLE
export const organisation = (state = initialState.organisation, action) => {
  switch (action.type) {
  case actions.FETCH_ORGANISATION_SUCCESS:
    return action.organisation
  default:
    return state
  }
}

export const organisationLoading = (state = initialState.organisationLoading, action) => {
  switch (action.type) {
  case actions.FETCH_ORGANISATION_SUCCESS:
  case actions.FETCH_ORGANISATION_FAILURE:
    return false
  case actions.FETCH_ORGANISATION_BEGIN:
    return true
  default:
    return state
  }
}