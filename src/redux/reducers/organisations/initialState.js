
export default{
    organisations: [],
    organisationsTotal: 0,
    organisationsLoading: false,
    organisationsFilters: {},
    organisationsOptions: {},
    organisation: {},
    organisationLoading: false,
}