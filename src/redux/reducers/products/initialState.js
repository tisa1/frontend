
export default{
    products: [],
    productsTotal: 0,
    productsLoading: false,
    productsFilters: {},
    productsOptions: {},
    product: {},
    productLoading: false,
}