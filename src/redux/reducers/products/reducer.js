import initialState from "./initialState"
import actions from "../../actions/products/names"
import _ from "underscore"

// PRODUCTS
export const products = (state = initialState.products, action) => {
  switch (action.type) {
  case actions.FETCH_PRODUCTS_SUCCESS:
    return action.products
  default:
    return state
  }
}

export const productsTotal = (state = initialState.productsTotal, action) => {
  switch (action.type) {
  case actions.FETCH_PRODUCTS_SUCCESS:
    return action.total
  default:
    return state
  }
}

export const productsLoading = (state = initialState.productsLoading, action) => {
  switch (action.type) {
  case actions.FETCH_PRODUCTS_BEGIN:
    return true
  case actions.FETCH_PRODUCTS_SUCCESS:
  case actions.FETCH_PRODUCTS_FAILURE:
    return false
  default:
    return state
  }
}

export const productsOptions = (state = initialState.productsOptions, action) => {
  switch (action.type) {
  case actions.UPDATE_PRODUCTS_OPTIONS:
    return _.extend(state, action.options)
  default:
    return state
  }
}

export const productsFilters = (state = initialState.productsFilters, action) => {
  switch (action.type) {
  case actions.UPDATE_PRODUCTS_FILTERS:
    return _.extend(state, action.filters)
  default:
    return state
  }
}

// PRODUCT SINGLE
export const product = (state = initialState.product, action) => {
  switch (action.type) {
  case actions.FETCH_PRODUCT_SUCCESS:
    return action.product
  default:
    return state
  }
}

export const productLoading = (state = initialState.productLoading, action) => {
  switch (action.type) {
  case actions.FETCH_PRODUCT_SUCCESS:
  case actions.FETCH_PRODUCT_FAILURE:
    return false
  case actions.FETCH_PRODUCT_BEGIN:
    return true
  default:
    return state
  }
}