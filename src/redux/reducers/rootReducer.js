import { combineReducers } from "redux";

// DEPRECATED
import cartReducer from "./cartReducer";
import * as globalReducers from "./globalReducers";
import * as organisationReducers from "./organisations/reducer";
import * as categoriesReducers from "./categories/reducer";
import * as productsReducer from "./products/reducer"
import * as cartItemsReducer from "./cart-items/reducer"
import wishlistReducer from "./wishlistReducer";
import shopReducer from "./shopReducer";

const rootReducer = combineReducers({

  // TODO: get rid of the deprecated reducers
  cartReducer,
  wishlistReducer,
  shopReducer,

  ...globalReducers,
  ...organisationReducers,
  ...categoriesReducers,
  ...productsReducer,
  ...cartItemsReducer,
});

export default rootReducer;
