import initialState from "./initialState"
import actions from "../../actions/cart-items/names"
import _ from "underscore"

// USER_CART_ITEMS
export const userCartItems = (state = initialState.userCartItems, action) => {
  switch (action.type) {
  case actions.FETCH_USER_CART_ITEMS_SUCCESS:
    return action.cartItems
  default:
    return state
  }
}

export const userCartItemsTotal = (state = initialState.userCartItemsTotal, action) => {
  switch (action.type) {
  case actions.FETCH_USER_CART_ITEMS_SUCCESS:
    return action.total
  default:
    return state
  }
}

export const userCartItemsLoading = (state = initialState.userCartItemsLoading, action) => {
  switch (action.type) {
  case actions.FETCH_USER_CART_ITEMS_BEGIN:
    return true
  case actions.FETCH_USER_CART_ITEMS_SUCCESS:
  case actions.FETCH_USER_CART_ITEMS_FAILURE:
    return false
  default:
    return state
  }
}

export const userCartItemsOptions = (state = initialState.userCartItemsOptions, action) => {
  switch (action.type) {
  case actions.UPDATE_USER_CART_ITEMS_OPTIONS:
    return _.extend(state, action.options)
  default:
    return state
  }
}

export const userCartItemsFilters = (state = initialState.userCartItemsFilters, action) => {
  switch (action.type) {
  case actions.UPDATE_USER_CART_ITEMS_FILTERS:
    return _.extend(state, action.filters)
  default:
    return state
  }
}