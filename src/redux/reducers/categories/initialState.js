
export default{
    categories: [],
    categoriesTotal: 0,
    categoriesLoading: false,
    categoriesFilters: {},
    categoriesOptions: {},
    category: {},
    categoryLoading: false,

    categoriesGraph: [],
    categoriesGraphTotal: 0,
    categoriesGraphLoading: false,

}