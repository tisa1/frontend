import initialState from "./initialState"
import actions from "../../actions/categories/names"
import _ from "underscore"

// CATEGORIES
export const categoriesGraph = (state = initialState.categoriesGraph, action) => {
  switch (action.type) {
  case actions.FETCH_CATEGORIES_GRAPH_SUCCESS:
    return action.categories
  default:
    return state
  }
}

export const categoriesGraphTotal = (state = initialState.categoriesGraphTotal, action) => {
  switch (action.type) {
  case actions.FETCH_CATEGORIES_GRAPH_SUCCESS:
    return action.total
  default:
    return state
  }
}

export const categoriesGraphLoading = (state = initialState.categoriesGraphLoading, action) => {
  switch (action.type) {
    case actions.FETCH_CATEGORIES_GRAPH_BEGIN:
      return true
    case actions.FETCH_CATEGORIES_GRAPH_SUCCESS:
    case actions.FETCH_CATEGORIES_GRAPH_FAILURE:
      return false
    default:
      return state
    }
}

export const categories = (state = initialState.categories, action) => {
  switch (action.type) {
  case actions.FETCH_CATEGORIES_SUCCESS:
    return action.categories
  default:
    return state
  }
}

export const categoriesTotal = (state = initialState.categoriesTotal, action) => {
  switch (action.type) {
  case actions.FETCH_CATEGORIES_SUCCESS:
    return action.total
  default:
    return state
  }
}

export const categoriesLoading = (state = initialState.categoriesLoading, action) => {
  switch (action.type) {
  case actions.FETCH_CATEGORIES_BEGIN:
    return true
  case actions.FETCH_CATEGORIES_SUCCESS:
  case actions.FETCH_CATEGORIES_FAILURE:
    return false
  default:
    return state
  }
}

export const categoriesOptions = (state = initialState.categoriesOptions, action) => {
  switch (action.type) {
  case actions.UPDATE_CATEGORIES_OPTIONS:
    return _.extend(state, action.options)
  default:
    return state
  }
}

export const categoriesFilters = (state = initialState.categoriesFilters, action) => {
  switch (action.type) {
  case actions.UPDATE_CATEGORIES_FILTERS:
    return _.extend(state, action.filters)
  default:
    return state
  }
}

// CATEGORY SINGLE
export const category = (state = initialState.category, action) => {
  switch (action.type) {
  case actions.FETCH_CATEGORY_SUCCESS:
    return action.category
  default:
    return state
  }
}

export const categoryLoading = (state = initialState.categoryLoading, action) => {
  switch (action.type) {
  case actions.FETCH_CATEGORY_SUCCESS:
  case actions.FETCH_CATEGORY_FAILURE:
    return false
  case actions.FETCH_CATEGORY_BEGIN:
    return true
  default:
    return state
  }
}