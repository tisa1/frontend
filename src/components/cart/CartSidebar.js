import { useState, useEffect, useCallback } from "react"
import { useSelector } from "react-redux";
import { Empty, Button } from "antd";
import Link from "next/link";
import React from "react";

import CartSidebarItem from "./CartSidebarItem";
import { calculateTotalPrice } from "../../common/shopUtils";
import { formatCurrency } from "../../common/utils";
import _ from "underscore"

function CartSidebar() {
  const [ total, setTotal ]=useState(0)
  const globalState = useSelector((state) => state.globalReducer);
  const userCartItems = useSelector((state) => state.userCartItems);
  const { currency, locales } = globalState.currency;

  useEffect(()=>{
    calculateTotal()
  },[userCartItems])

  const calculateTotal=useCallback(() => {
    let total = 0
    for(let item of userCartItems){
      total += item?.quantity * item?.product?.data?.price
    }
    setTotal(total)
  },[userCartItems])

  return userCartItems?.length === 0 ? (
    <Empty description="No products in cart" />
  ) : (
    <div className="cart-sidebar">
      <div className="cart-sidebar-products">
        {userCartItems.map((item, index) => (
          <CartSidebarItem key={index} item={item} />
        ))}
      </div>
      <div className="cart-sidebar-total">
        <h5>
          Total:{" "}
          <span>
            {formatCurrency(total, locales, currency)}
          </span>
        </h5>
        <div className="cart-sidebar-total__buttons">
          <Button type="primary" shape="round">
            <Link href={process.env.PUBLIC_URL + "/shop/checkout"}>
              <a>Checkout</a>
            </Link>
          </Button>
        </div>
      </div>
    </div>
  );
}

export default React.memo(CartSidebar);
