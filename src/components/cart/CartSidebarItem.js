import React, { useState } from "react"
import Link from "next/link"
import { Modal, message } from "antd"
import { useSelector, useDispatch } from "react-redux"
import { formatCurrency } from "../../common/utils"
import QuantitySelector from "../controls/QuantitySelector"
import { getUserCartItems } from "redux/actions/cart-items/actions"
import {
  CREATE_CART_ITEM as createCartItemUrl,
  DELETE_CART_ITEM as deleteCartItemUrl,
} from "lib/network/endpoints"
import {
  POST, DELETE,
} from "lib/network/requests"

function CartSidebarItem({ item={} }) {
  const data = item?.product?.data

  const dispatch = useDispatch()
  const [visible, setVisible] = useState(false)
  const globalState = useSelector((state) => state.globalReducer)
  const { currency, locales } = globalState.currency
  
  const onRemoveProductFromCart = (e) => {
    e.preventDefault()
    showModal()
  }
  const showModal = () => {
    setVisible(true)
  }

  const onConfirm = () => {
    DELETE({url: deleteCartItemUrl, data:{ cartItemId: item?._id }}, (err,res)=>{
      if(err){
        return message.error(err?.message)
      }
      dispatch(getUserCartItems())
      setVisible(false)
      return message.success(res?.message)
    })
    return message.error("Product removed from cart")
  }

  const handleCancel = (e) => {
    setVisible(false)
  }

  const onIncrease=(quantity)=>{
    POST({url: createCartItemUrl, data:{ productId: item?.product?._id, quantity }}, (err,res)=>{
      if(err){
        return message.error(err?.message)
      }
      dispatch(getUserCartItems())
      return message.success(res?.message)
    })
  }

  return (
    <>
      <div className="cart-sidebar-item">
        <div className="cart-sidebar-item__image">
          <img src={data?.thumbImage?.[0]} alt="Product image" />
        </div>
        <div className="cart-sidebar-item__content">
          <Link
            href={process.env.PUBLIC_URL + `/product/[slug]`}
            as={process.env.PUBLIC_URL + `/product/${data?.slug}`}
          >
            <a>{data?.name}</a>
          </Link>
          <h5>
            {data?.discount
              ? formatCurrency(
                  (data?.price - data?.discount) * data?.quantity,
                  locales,
                  currency
                )
              : formatCurrency(
                  data?.price * item?.quantity,
                  locales,
                  currency
                )}
          </h5>
          <QuantitySelector
            size="small"
            defaultValue={item?.quantity}
            min={1}
            max={100}
            onDecrease={()=>onIncrease(-1)}
            onIncrease={()=>onIncrease(1)}
          />
        </div>
        <div className="cart-sidebar-item__close">
          <a href="#" onClick={onRemoveProductFromCart}>
            <i className="icon_close" />
          </a>
        </div>
      </div>
      <Modal
        title="Cofirm this action"
        visible={visible}
        onOk={onConfirm}
        onCancel={handleCancel}
      >
        <p>Are your sure to remove product from cart ?</p>
      </Modal>
    </>
  )
}

export default React.memo(CartSidebarItem)
