import Home from "modules/home/components/Home"
import {
  connect,
} from "react-redux"
import {
} from "redux/actions/app"

const mapStateToProps = (state, ownProps) => ({
})

const mapDispatchToProps = (dispatch) => ({
})

const AppContainer = connect(
  mapStateToProps,
  mapDispatchToProps,
)(Home)

export default AppContainer
