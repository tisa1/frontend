import React, { useState } from "react"
import { useSelector, useDispatch } from "react-redux";
import { Row, Col, Spin } from 'antd'
import { Form, Input, Button, Checkbox } from 'antd';
import { UserOutlined, LockOutlined } from '@ant-design/icons';
import notification from "lib/helpers/notification"
import { POST } from "lib/network/requests"
import { SIGN_IN as signinUrl } from "lib/network/endpoints"
import { setUserToken } from "redux/actions/userActions";

const Logare = () => {
    const dispatch = useDispatch()
    const [ loading, setLoading ] = useState(false)

    const onFinish = (data) => {
        setLoading(true)
        POST({url: signinUrl, data }, (err,res)=>{
            setLoading(false)
            if(err){
                return notification.error(err?.message)
            }
            dispatch(setUserToken(res.token))
            notification.success(res?.message)
            return window.location.href="/"
        })
    }
      
    return(
        <>
            <Row style={{marginTop:"20%"}}>
                <Col span={8}></Col>
                <Col span={8}>
                    <Form
                        name="normal_login"
                        className="login-form"
                        initialValues={{ remember: true }}
                        onFinish={onFinish}
                        >
                        <Form.Item
                            name="email"
                            rules={[{ type:"email", required: true, message: 'Completează email!' }]}
                        >
                            <Input prefix={<UserOutlined className="site-form-item-icon" />} placeholder="Email" />
                        </Form.Item>
                        <Form.Item
                            name="password"
                            rules={[{ required: true, message: 'Completează parola!' }]}
                        >
                            <Input
                            prefix={<LockOutlined className="site-form-item-icon" />}
                            type="password"
                            placeholder="Parola"
                            />
                        </Form.Item>
                        <Form.Item>
                            <Row>
                                <Col span={12}>
                                    <Form.Item name="remember" valuePropName="checked" noStyle>
                                        <Checkbox>Ține-mă minte</Checkbox>
                                    </Form.Item>
                                </Col>
                                
                                <Col style={{textAlign:'right'}} span={12}>
                                    <a className="login-form-forgot" href="">
                                        Am uitat parola
                                    </a>
                                </Col>
                            </Row>
                        </Form.Item>

                        <Form.Item>
                            <Button 
                                block 
                                type="primary" 
                                htmlType="submit" 
                                className="login-form-button"
                            >
                                {loading?<Spin/>:"Autentificare"}
                            </Button>
                            <div style={{textAlign:"center", marginTop:"2%"}}>
                                Sau <a href="/inregistrare"> înregistrează-te acum!</a>
                            </div>
                        </Form.Item>
                    </Form>
                </Col>
                <Col span={8}></Col>
            </Row>
        </>
    )
}

export default Logare