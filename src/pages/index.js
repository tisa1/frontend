import { useEffect } from "react"
import { useSelector, useDispatch,  } from "react-redux";
import { useRouter } from "next/router";
import LayoutOne from "../components/layouts/LayoutOne";
import ShopLayout from "../components/shop/ShopLayout";
import productData from "../data/product.json";
import useProductData from "../common/useProductData";
import { getCategoriesGraph } from "redux/actions/categories/actions";
import { getProducts } from "redux/actions/products/actions"
import { getUserCartItems } from "redux/actions/cart-items/actions"

export default function Home() {
  const dispatch = useDispatch()

  const router = useRouter();
  const globalState = useSelector((state) => state.globalReducer);
  const data = useProductData(
    productData,
    globalState.category,
    router.query.q
  )

  useEffect(()=>{
    dispatch(getCategoriesGraph())
    dispatch(getProducts())
    dispatch(getUserCartItems())
  })

  return (
    <LayoutOne title="Tisa" containerType="fluid" headerStyle="two">
      <ShopLayout
        shopSidebarResponsive={{ xs: 24, lg: 4 }}
        shopContentResponsive={{ xs: 24, lg: 20 }}
        productResponsive={{ xs: 12, sm: 8, md: 6, xxl: 4 }}
        productPerPage={18}
        data={[...data]}
        containerType="fluid"
      />
    </LayoutOne>
  );
}
