import React, { useState } from "react"
import { useDispatch } from "react-redux";
import { Row, Col, Spin } from 'antd'
import { Form, Input, Button, Checkbox } from 'antd'
import { UserOutlined, LockOutlined } from '@ant-design/icons'
import { POST } from "lib/network/requests"
import { SIGN_UP as signupUrl } from "lib/network/endpoints"
import notification from "lib/helpers/notification"
import { setUserToken } from "redux/actions/userActions";

const Logare = () => {
    const dispatch = useDispatch();
    const [loading, setLoading] = useState(false)

    const onFinish = (data) => {
        setLoading(true)
        POST({url: signupUrl, data }, (err,res)=>{
            setLoading(false)
            if(err){
                return notification.error(err?.message)
            }

            dispatch(setUserToken(res.token));
            notification.success(res?.message);
            return window.location.href="/"
        })
    };
      
    return(
        <>
            <Row style={{marginTop:"20%"}}>
                <Col span={8}></Col>
                <Col span={8}>
                    <Form
                        name="normal_login"
                        className="login-form"
                        initialValues={{ remember: true }}
                        onFinish={onFinish}
                        >
                        <Form.Item
                            name={["profile","firstName"]}
                            rules={[{ type:"string", required: true, message: 'Completează numele' }]}
                        >
                            <Input placeholder="Numele" />
                        </Form.Item>
                        <Form.Item
                            name={["profile","lastName"]}
                            rules={[{ type:"string", required: true, message: 'Completează prenumele' }]}
                        >
                            <Input placeholder="Prenumele" />
                        </Form.Item>
                        <Form.Item
                            name="email"
                            rules={[{ type:"email", required: true, message: 'Completează email valid' }]}
                        >
                            <Input prefix={<UserOutlined className="site-form-item-icon" />} placeholder="Email" />
                        </Form.Item>
                        <Form.Item
                            name="password"
                            rules={[
                                { required: true, message: 'Completează parola' },
                                ({  }) => ({
                                    validator(_, value) {
                                        if(!value){
                                            return Promise.resolve();
                                        }
                                        if(value?.length<8){
                                            return Promise.reject(new Error('Parola prea scurtă'));

                                        }
                                        if((!/[A-Z]/.test(value))){
                                            return Promise.reject(new Error('Parola nu conține litere mari'));

                                        }
                                        if(!(/[a-z]/.test(value))){
                                            return Promise.reject(new Error('Parola nu conține litere mici'));
                                        }
                                        if(!(/[-+_!@#$%^&*.,?]/.test(value))){
                                            return Promise.reject(new Error('Parola nu conține simboluri'));
                                        }
                                        return Promise.resolve()
                                    },
                                  })
                            ]}
                        >
                            <Input
                            prefix={<LockOutlined className="site-form-item-icon" />}
                            type="password"
                            placeholder="Parola"
                            />
                        </Form.Item>
                        <Form.Item
                            name="password-check"
                            rules={[
                                { required: true, message: 'Completează parola' },
                                ({ getFieldValue }) => ({
                                    validator(_, value) {
                                        if(!value){
                                            return Promise.resolve();
                                        }
                                        if (getFieldValue('password') !== value) {
                                            return Promise.reject(new Error('Parolele nu coincid'));
                                        }
                                        return Promise.resolve()
                                    },
                                  })
                            ]}
                        >
                            <Input
                            prefix={<LockOutlined className="site-form-item-icon" />}
                            type="password"
                            placeholder="Repetă parola"
                            />
                        </Form.Item>
                        <Form.Item>
                            <Row>
                                <Col span={12}>
                                    <Form.Item name="remember" valuePropName="checked" noStyle>
                                        <Checkbox>Ține-mă minte</Checkbox>
                                    </Form.Item>
                                </Col>
                            </Row>
                        </Form.Item>

                        <Form.Item>
                            <Button
                                block 
                                type="primary" 
                                htmlType="submit" 
                                className="login-form-button"
                                disabled={loading}
                            >
                                {loading?<Spin/>:"Înregistrare"}
                            </Button>
                            <div style={{textAlign:"center", marginTop:"2%"}}>
                                Sau <a href="/logare"> Autentifică-te acum!</a>
                            </div>
                        </Form.Item>
                    </Form>
                </Col>
                <Col span={8}></Col>
            </Row>
        </>
    )
}

export default Logare