import {
  Form,
  Input,
  Button,
  Row,
  Col,
  Spin,
} from "antd"
import { useState, useCallback } from "react"
import Slider from "react-slick"
import { useRouter } from "next/router"
import { useSelector } from "react-redux"
import Link from "next/link"

import LayoutOne from "../../../components/layouts/LayoutOne"
import Container from "../../../components/other/Container"
import { POST } from "lib/network/requests"
import { CREATE_ORGANISATION as createOrgUrl } from "lib/network/endpoints"
import notification from "lib/helpers/notification"

const OrganisationCreate = () => {
  const router = useRouter()
  const [loading, setLoading ] = useState(false)

  const onFinish = (data) => {
    setLoading(true)
    POST({ url:createOrgUrl, data }, ( err, res )=>{
      setLoading(false)
      if(err){
        return notification.error(err?.message)
      }
      notification.success(res?.message)
      return router.push("/profil/organizatii")

    })
  }

  const onFinishFailed = (errorInfo) => {
  }


  return (
    <LayoutOne headerStyle="two" title="Checkout">
      <div className="checkout">
        <div className="checkout-top">
          <Container>
            <Row>
              <Col md={8}></Col>
              <Col md={8}>
                <h3 className="checkout-title">Detaliile organizației tale</h3>
                <Form
                  name="basic"
                  onFinish={onFinish}
                  onFinishFailed={onFinishFailed}
                  id="checkout-form"
                  layout="vertical"
                  className="checkout-form"
                >
                  <Form.Item
                      label="Numele"
                      name="name"
                      rules={[
                      {
                          required: true,
                          message: "Obligator!",
                      },
                      ]}
                  >
                      <Input />
                  </Form.Item>
                  <Button
                      type="submit"
                      htmlType="submit"  
                      block
                  >
                    {loading?<Spin/>:"Creare"}
                  </Button>
                </Form>
              </Col>
              <Col md={8}></Col>
          </Row>
          </Container>
        </div>
      </div>
    </LayoutOne>
  );
}

export default OrganisationCreate