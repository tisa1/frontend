import {
  List,
  Popover,
  Skeleton,
  Avatar,
  Button,
  Spin,
} from "antd"
import { useState, useEffect } from "react"
import LayoutOne from "../../../components/layouts/LayoutOne"
import Container from "../../../components/other/Container"
import { GET, DELETE } from "lib/network/requests"
import { 
  FETCH_USER_ORGANISATIONS as fetchUserOrganisationsUrl,
  DELETE_ORGANISATION as deleteOrganisationUrl,
} from "lib/network/endpoints"
import notification from "lib/helpers/notification"
import moment from "moment"
import _ from "underscore"

const OrganisationCreate = () => {
  const [ loading, setLoading ] = useState(false)
  const [ organisations, setOrganisations ] = useState([])
  const [ orgsBeingDeleted, setOrgsBeingDeleted ] = useState([])
  const [ visiblePopover, setVisiblePopover ] = useState(null)

  useEffect(()=>{
    getOrganisations()
  },[])

  const getOrganisations = () =>{
    setLoading(true)
    GET({ url:fetchUserOrganisationsUrl },(err,res)=>{
      setLoading(false)
      if(err){
        return notification.error(err?.message)
      }
      return setOrganisations(res?.organisations)
    })
  }

  const onDeleteOrg=(organisationId)=>{
    setOrgsBeingDeleted(_.union(orgsBeingDeleted,[organisationId]))
    setVisiblePopover(null)

    DELETE({ url:deleteOrganisationUrl, data:{organisationId} },(err,res)=>{
      if(err){
        return notification.error(err?.message)
      }
      getOrganisations()
      setOrgsBeingDeleted(_.without(orgsBeingDeleted,organisationId))
      return notification.success(res?.message)
    })
  }

  const getContent=(organisationId)=>{
    return(
      <Button onClick={()=>onDeleteOrg(organisationId)} danger>Da</Button>
    )
  }

  const handleVisibleChange = orgId => {
    if(orgId === visiblePopover){
      orgId = null
    }

    setVisiblePopover(orgId)
  };

  return (
    <LayoutOne headerStyle="two" title="Organizatii">
      <div className="checkout">
        <div className="checkout-top">
          <Container>
                <h3 className="checkout-title">Organizațiile tale</h3>
                <List
                  loading={loading}
                  itemLayout="horizontal"
                  dataSource={organisations}
                  renderItem={(item,idx) => (
                    <List.Item
                      actions={[
                        <Popover 
                          content={getContent(item?.organisation?._id)} 
                          visible={visiblePopover === item?.organisation?._id}
                          onVisibleChange={()=>handleVisibleChange(item?.organisation?._id)}
                          title="Ești sigur?" 
                          trigger="click"
                        >
                          <Button 
                            disabled={_.includes(orgsBeingDeleted, item?.organisation?._id)} 
                            type="danger" 
                            key="delete"
                          >
                            {
                              _.includes(orgsBeingDeleted, item?.organisation?._id)
                              ? <Spin/>
                              : "Șterge"
                            }
                          </Button>
                        </Popover>
                      , 
                    ]}
                    >
                      <Skeleton avatar title={false} loading={item.loading} active>
                        <List.Item.Meta
                          avatar={
                            <Avatar style={{backgroundColor:"#fb5231"}}>{idx+1}</Avatar>
                          }
                          title={<a href={`/profil/organizatii/${item?.organisation?._id}`}>{item?.organisation?.name}</a>}
                          description={moment(item?.createdAt).format('MMMM Do YYYY, h:mm:ss a')}
                        />
                        {/* <div>content</div> */}
                      </Skeleton>
                    </List.Item>
                  )}
                />
                <div style={{textAlign:"center"}}>
                  <Button onClick={()=>window.location.href="/profil/organizatii/creare"}>Crează organizație</Button>
                </div>
          </Container>
        </div>
      </div>
    </LayoutOne>
  );
}

export default OrganisationCreate