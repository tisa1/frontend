import {
  Form,
  Input,
  Button,
  Row,
  Col,
  Spin,
  Select,
  InputNumber,
} from "antd"
import { useState, useCallback, useEffect } from "react"
import { useRouter } from "next/router"
import LayoutOne from "../../../../components/layouts/LayoutOne"
import Container from "../../../../components/other/Container"
import { POST, GET } from "lib/network/requests"
import { 
  CREATE_PRODUCT as createUrl,
  FETCH_CATEGORY_OPTS as fetchCatOptsUrl,
  FETCH_SUBCATEGORIES_BY_CATEGORY_OPTS as fetchSubcatOptsUrl,
  FETCH_TEMPLATE_BY_SUBCATEGORY as fetchTempBySubcatUrl
 } from "lib/network/endpoints"
import notification from "lib/helpers/notification"
import _ from "underscore"

const { Option } = Select
const { TextArea } = Input

const fieldTypes = {
  STRING: "string",
  TEXT: "text",
  NUMBER: "number",
  YEAR: "year",
  PRICE: "price",
  SELECT: "select",
}

const OrganisationCreate = () => {
  const router = useRouter()
  const { orgId } = router.query

  const [loading, setLoading ] = useState(false)
  const [categoryOpts, setCategoryOpts] = useState([])
  const [categoryId, setCategoryId] = useState(null)
  const [subcategoryId, setSubcategoryId] = useState(null)
  const [subcatOpts, setSubcatOpts] = useState([])
  const [template, setTemplate] = useState([])

  useEffect(() =>{
    if(!categoryId) return
    getSubcategoryOpts()
  },[categoryId, getSubcategoryOpts])

  useEffect(() =>{
    if(!subcategoryId) return
    getTemplate()
  },[subcategoryId, getTemplate])

  useEffect(() =>{
    getCategoryOpts()
  },[getCategoryOpts])

  const getCategoryOpts=useCallback(()=>{
    GET({ url:fetchCatOptsUrl },(err,res)=>{
      if(err){
          return notification.error(err?.message)
      }
      setCategoryOpts(res.options)
    })
  },[])

  const getSubcategoryOpts = useCallback(() => {
    GET({ url:fetchSubcatOptsUrl, data:{ categoryId } },(err,res)=>{
      if(err){
          return notification.error(err?.message)
      }
      setSubcatOpts(res.options)
    })
  },[categoryId])

  const onFinish = (data) => {
    _.extend(data, {organisationId: orgId})
    setLoading(true)
    POST({ url:createUrl, data }, ( err, res )=>{
      setLoading(false)
      if(err){
        return notification.error(err?.message)
      }
      notification.success(res?.message)
    })
  }

  const onFinishFailed = (errorInfo) => {
  }

  const onCategorySelect=(categoryId)=>{
    setCategoryId(categoryId)
  }

  const onSubcategorySelect = (subcategoryId)=>{
    setSubcategoryId(subcategoryId)
  }

  const getTemplate=()=>{
    GET({ url:fetchTempBySubcatUrl, data:{ subcategoryId } },(err,res)=>{
      if(err){
          return notification.error(err?.message)
      }
      setTemplate(res?.template)
    })
  }

  const renderInput=(field)=>{
    switch (field?.input?.dataType) {
      case fieldTypes.TEXT:
        return <Form.Item
            name={["data", field?.input?.name]}
            rules={[{ required: true, message: "Obligator" }]}
          >
            <TextArea
              placeholder={field?.input?.name}
            />
          </Form.Item>

      case fieldTypes.SELECT:
        return <Form.Item 
            name={["data", field?.input?.name]}
            rules={[{ required: true, message: 'Alege optiunea!'  }]}
          >
            <Select
              placeholder={field?.input?.name}
              allowClear
            >
              {
                _.map(field?.input?.meta?.select?.options,i=>(
                  <Option value={i}>{i}</Option>
                ))
              }
            </Select>
          </Form.Item>
      case fieldTypes.PRICE:
        return <Form.Item 
            name={["data", field?.input?.name]}
            rules={[{ required: true, message: 'Completeaza camp!'  }]}
          >
            <InputNumber style={{width:"100%"}} placeholder={field?.input?.name} min={0} max={1000000}/>
          </Form.Item>
      case fieldTypes.NUMBER:
        return <Form.Item 
            name={["data", field?.input?.name]}
            rules={[{ required: true, message: 'Completeaza camp!'  }]}
          >
            <InputNumber style={{width:"100%"}} placeholder={field?.input?.name}/>
          </Form.Item>
      case fieldTypes.YEAR:
        return <Form.Item 
            name={["data", field?.input?.name]}
            rules={[{ required: true, message: 'Completeaza camp!'  }]}
          >
            <InputNumber min={1500} max={2500} style={{width:"100%"}} placeholder={field?.input?.name}/>
          </Form.Item>
      default:
        return <Form.Item
            name={["data", field?.input?.name]}
            rules={[{ required: true, message: "Obligator" }]}
          >
            <Input
              placeholder={field?.input?.name}
            />
          </Form.Item>
    }
    
  }

  return (
    <LayoutOne headerStyle="two" title="Creare produs">
      <div className="checkout">
        <div className="checkout-top">
          <Container>
            <Row>
              <Col md={8}></Col>
              <Col md={8}>
                <h3 className="checkout-title">Detaliile produsului</h3>
                <Form
                  name="basic"
                  onFinish={onFinish}
                  onFinishFailed={onFinishFailed}
                  id="checkout-form"
                  layout="vertical"
                  className="checkout-form"
                >
                  <Form.Item name="category" rules={[{ required: true, message: 'Alege categoria!'  }]}>
                    <Select
                      onChange={onCategorySelect}
                      placeholder="Categoria"
                      allowClear
                    >
                      {
                        _.map(categoryOpts,i=>(
                          <Option value={i?._id}>{i?.name}</Option>

                        ))
                      }
                    </Select>
                  </Form.Item>

                  {
                    categoryId
                    && <Form.Item name="subcategoryId" rules={[{ required: true, message: 'Alege subcategoria!'  }]}>
                      <Select
                        placeholder="Subcategoria"
                        allowClear
                        onChange={onSubcategorySelect}

                      >
                        {
                          _.map(subcatOpts,i=>(
                            <Option value={i?._id}>{i?.name}</Option>
                          ))
                        }
                      </Select>
                    </Form.Item>
                  }

                  {/* Dynamic fields rendering */}
                  {
                    template &&
                    _.map(template, input=>renderInput(input))
                  }
                  <Button
                      type="submit"
                      htmlType="submit"  
                      block
                  >
                    {loading?<Spin/>:"Creare"}
                  </Button>
                </Form>
              </Col>
              <Col md={8}></Col>
          </Row>
          </Container>
        </div>
      </div>
    </LayoutOne>
  );
}

export default OrganisationCreate