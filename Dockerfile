FROM node

WORKDIR /opt/frontend

# Installing dependencies
COPY package.json .
COPY yarn.lock .

RUN yarn

COPY . .
RUN yarn build

ENV PORT=3000

CMD [ "yarn", "start" ]

